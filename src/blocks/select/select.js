$('.select__item').select2({
  minimumResultsForSearch: -1,
  containerCssClass: 'select__btn',
  dropdownCssClass: 'select__dropdown',
  width: 'auto',
  dropdownAutoWidth: true
});