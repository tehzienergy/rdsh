if (screen.width < 576) {
  $(".main__section-close").click(function(e) {
    e.preventDefault();
    $(this).css('opacity', '0');
  });

  $(".main__section").click(function() {
    $(".main__section-close").css('opacity', '1');
  });
}

function mainPreload() {
  var i = 0;
  $('.main__section').each(function() {
    var src = $(this).css('background-image');
    var url = src.match(/\((.*?)\)/)[1].replace(/('|")/g,'');

    var img = new Image();
    img.onload = function() {
      i+=1;
      if (i==3) {
        $('.main__preloader').hide();
      }
    }
    img.src = url;
    if (img.complete) img.onload();
  });
}

mainPreload();