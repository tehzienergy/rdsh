document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar-new');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    selectable: true,
    locale: 'ru',
    headerToolbar: {
      left: '',
      center: '',
      right: ''
    },
    dateClick: function(info) {
      alert('clicked');
    },
    select: function(info) {
      alert('selected');
    }
  });

  calendar.render();
});

const ps = new PerfectScrollbar('.calendar-new__dscr-content');